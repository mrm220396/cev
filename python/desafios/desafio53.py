""" Jan Mesu 01/03/2020 
Crie um programa que leia uma frase e diga se ela é um palindromo
"""
import sys

original = str(input('Escreva um frase para ver se ela é um polindromo ~$ '))
frase = original.replace(" ", "").lower()

if frase[::-1] == frase:
    print(f'A Frase {original} ao inverso fica {frase[::-1]} é um Palindrimo')
    sys.exit()
print(f'A Frase {original} ao inverso fica {frase[::-1]} não é um Palindrimo')
