""" Jan Mesu 01/04/2020 
Faça um programa que pesa o nome, idade, sexo
e no final diga

A média de idade do grupo
Diga o nome do homem mais velho
Quantas mulheres tem menos de 21 anos
"""

media = 0
oldman = ''
oldmanage = 0
youngwomen = 0

for i in range(1,5):
    print(f'--- {i}° Pessoa ---\n')
    nome  = str(input('Escreva o seu nome '))
    idade = int(input('Escreva a sua idade '))
    sexo  = str(input('Difite o seu sexo M/F ')).lower()
    if sexo == 'm' and idade > oldmanage: 
       oldman = nome
    elif sexo == 'f' and idade < 21: 
         youngwomen += 1
    media += idade
media /= 10   
print(f'A média de idade foi {media}\nO homem mais velho se chama {oldman}\nE as mulheres menores de 21 anos totalizaram {youngwomen}')