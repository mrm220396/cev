""" Jan Mesu 01/03/2020 
faça um programa que leia um número inteiro e diz se ele é 
primo ou não
"""
numero = int(input('Escreva um número para saber se ele é primo ~$ '))
divisors = 0
for i in range(1, numero+1):
    if numero % i == 0:
        print(f'\033[35m{i}', end=' ')
        divisors += 1
    else:
        print(f'\033[33m{i}', end=' ')
if divisors > 2 or numero < 2:
   print(f'\033[m{numero} não é um número primo ')
else:
   print(f'\033[m{numero} é um número primo ')  