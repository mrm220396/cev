""" Jan Mesu 01/04/2020 
Faça um programa que calcule a soma de todos os números impares
que são multiplos de 3 e que se encontrem entre 1 até 500
"""
soma = 0
counter = 0
for c in range(1,500,2):
    if c % 3 == 0:
       counter += 1 
       soma += c
print(f'A soma de todos os números múltiplos de 3 de 1 até 500 é {counter} e a soma foram {soma}')