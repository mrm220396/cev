""" Jan Mesu 01/03/2020 
Crie um programa que leia o ano de nascimento de 7 pessoas
e diga quandas delas atingiram a maior idade 21 ou não
"""
from datetime import datetime
maior = 0
menor = 0
for i in range(0,7):
    n = int(input('Qual é o ano de nascimento? '))
    n = datetime.now().year - n
    if n >= 21:
       maior += 1
    else:
       menor += 1   
print(f'Os maiores de 21 anos totalizaram {maior}\nOs menores totalizaram {menor}')