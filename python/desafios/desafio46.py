""" jan Mesu 01/04/2020
Faça um programa que escreva a contagem regressiva para o estouro de fogos
de artifício, indo de 10 até 1
"""
from time  import sleep
from emoji import emojize

for c in range(10, -1,-1):
    sleep(1)
    print(c)
print(emojize('Fogos!!  :fireworks:  :fireworks: :fireworks:', use_aliases=True))