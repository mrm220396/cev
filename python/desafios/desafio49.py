""" Jan Mesu 01/03/2020 
Faça a tabuada com for igual ao exercício 9
"""

numero = int(input('Escreva um número para ver a sua tabuada ~$ '))

for c in range(0,11):
    print(f'{numero} x {c:2} = {numero*c}')