""" Jan Mesu 01/03/2020 
Escreva um programa que leia um termo e diga a P.A dela
"""
primeiro = int(input('Escreva o primeiro termo '))
razao = int(input('Escreva a razão '))
decimo = primeiro + (10-1) * razao

for c in range(primeiro, decimo+razao, razao):
    print(f'{c} -> ', end=' ')
print('Acabou')